SEPA Plugin for Shopware 6
This plugin provides a simple way to collect SEPA payment information. It will inject SEPA form fields into the chekout process and into the customer backend section. It will store the SEPA information to each order and will also store the information directly to the customer account. The user can change the information inside the user section. This plugin comes with the AGPLv3 license.

Warning and Disclaimer
SEPA information is extremely sensitive data. It is your responsibility to ensure that the software and hardware used (server, operating system, shop software, etc...) meets the latest applicable security requirements. The SEPA data is stored unencrypted inside your database. You should delete this data regularly. The manufacturer of the plugin is not responsible for damage (e.g. data theft) caused by exploiting security vulnerabilities of the shop, the server, the operating system or inside this plugin.

Further Instructions will follow!